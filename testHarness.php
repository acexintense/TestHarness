<?php
#!/usr/bin/php -q
class TestHarness { 

	public function main(){
	
	}

} 

array_shift($argv);
$method = array_shift($argv);

$testHarness = new TestHarness;

if(!method_exists($testHarness, $method)){
	exit("Method $method doesnt exist!\n");
}

$reflection = new ReflectionMethod(TestHarness::class, $method);
$reqParams = $reflection->getNumberOfRequiredParameters();

if(count($argv) != $reqParams){
	exit("The method $method requires $reqParams parameters!\n");
}

print($testHarness->{$method}(...$argv) . "\n");
